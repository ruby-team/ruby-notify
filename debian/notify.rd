=begin
=NAME

notify - desktop notification on cross platforms

=SYNOPSIS

 notify  [OPTION...] <SUMMARY> [BODY] 

=DESCRIPTION

The notify provides desktop notification on cross platforms.
In Debian, notify execute notify-send command. 

=AUTHORS

This manual page was written by Youhei SASAKI <uwabami@gfd-dennou.org>,
for the Debian GNU/Linux system(but may be used by others).
